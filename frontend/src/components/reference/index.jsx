import styled from 'styled-components'

export const ReferenceWrapper = styled.div`
  width: 100%;
  padding: 4px;
  margin: 4px 0;
  &:last-child { border-bottom: none; }
`
import React     from 'react';
import hamburger from './static/menu.svg';
import close     from './static/close.svg';
import logo      from './static/logo2.svg';
import { Dropdown, DropDownContent, HamburgerIcon, Nav, NavContainer, NavLogo, NavLink, DesktopMenu, MobileMenu } from './MenuElements';

function Navbar() {
  const [showMenu, setShowMenu] = React.useState(false)

  // Hide oe show dropdown menu on mobile
  function HandleShowMenu() {
    const el = document.getElementById("dropdown")
    if (!showMenu) {
      el.style.opacity = 1
      el.style.pointerEvents = 'inherit'
      return setShowMenu(true)
    }
    if (showMenu) {
      el.style.opacity = 0
      el.style.pointerEvents = 'none'
      return setShowMenu(false)
    }
  }

  return (
    <>
      <Nav>
        <NavContainer>
          <NavLogo to='/'><img src={logo} alt="Software for a better society"/></NavLogo>
          <DesktopMenu>
            <NavLink to='/'><h6>Home</h6></NavLink>
            <NavLink to='/phd'><h6>PhD & Postdoc</h6></NavLink>
            <NavLink to='/master'><h6>Master</h6></NavLink>
            <NavLink to='/teaching'><h6>Teaching</h6></NavLink>
            <NavLink to='/team'><h6>Team</h6></NavLink>
            <NavLink to='/projects'><h6>Projects</h6></NavLink>
            <NavLink to='/videos'><h6>Videos</h6></NavLink>
            <NavLink to='/cooperators'><h6>Cooperators</h6></NavLink>
          </DesktopMenu>
          <MobileMenu onClick={HandleShowMenu}>
            { showMenu ? <><HamburgerIcon src={close} alt='menu'></HamburgerIcon>Close</> : <><HamburgerIcon src={hamburger} alt='close'></HamburgerIcon>Menu</> }
          </MobileMenu>
        </NavContainer>
      </Nav>
      <Dropdown>
        <DropDownContent>
          <NavLink onClick={HandleShowMenu} to='/phd'><h6>PhD and Postdoctoral Students</h6></NavLink>
          <NavLink onClick={HandleShowMenu} to='/master'><h6>Master Students</h6></NavLink>
          <NavLink onClick={HandleShowMenu} to='/teaching'><h6>Teaching</h6></NavLink>
          <NavLink onClick={HandleShowMenu} to='/team'><h6>Team</h6></NavLink>
          <NavLink onClick={HandleShowMenu} to='/projects'><h6>Projects</h6></NavLink>
          <NavLink onClick={HandleShowMenu} to='/videos'><h6>Videos</h6></NavLink>
          <NavLink onClick={HandleShowMenu} to='/cooperators'><h6>Cooperators</h6></NavLink>
        </DropDownContent>
      </Dropdown>
    </>
  )
}

export default Navbar
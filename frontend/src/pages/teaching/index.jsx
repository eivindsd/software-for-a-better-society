import React                     from 'react';
import { Navigation }            from '../../components/globals/Navigation';
import { Subject, CustomButton } from './elements';
import { Wrapper, Row, H1, CustomWidthWrapper } from '../../components/globals';

export default function Teaching() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/teaching">teaching</a></Navigation>
          <H1>Teaching</H1>
          <Subject href='/tdt4290'>
            <h3>TDT4290 - Customer Driven Project</h3>
            <p>Each group is given a task from a client that is to be carried out as a project. All phases of a development project are to be covered: Preliminary studies, requirements specification, design, implementation, and evaluation.</p>
            <CustomButton>Read more</CustomButton>
          </Subject>
          <Subject href='/tdt10'>
            <h3>TDT10-Gender and diversity in software development</h3>
            <p>The purpose of this course is to investigate why and how increased understanding of the role of gender and diversity can contribute to better knowledge, processes, and solutions for Software Development.</p>
            <CustomButton>Read more</CustomButton>
          </Subject>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
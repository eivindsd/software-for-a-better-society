import React from "react";
import { Subheading } from '../../components/globals';

export const MasterSetup = (
  <>
    <Subheading>Project and Master theses 2021-2022</Subheading>
    <p> See <a href="https://www.idi.ntnu.no/education/masteroppgaver.php?p1_1=1&f73=1&s=2" target="_blank" rel="noopener noreferrer">descriptions in NTNU’s system</a></p>
    <p> See also Module <a href="/tdt10" target="_blank" rel="noopener noreferrer"> TDT10 Gender and diversity in software development</a></p>
  </>
)

export const Spring22 = (
  <>
    <Subheading>Master Projects 2022 </Subheading>
    <ul>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/miabjo_ntnu_no/EWsK0al-skdHj5kLZ1ALoUYB5RPer4-mqISB1kxdqQkmRw?e=VUeCsK" target="_blank" rel="noopener noreferrer">Designing Software to Raise Cyber Security Awareness in Developing Countries</a>, Giske Naper Freberg, June 2022, Master Thesis.</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/miabjo_ntnu_no/EbfInztiRBNOu2Ol9ww1b8wB6UOHuXCsOTRVXzIV-G5goA?e=gds2zz" target="_blank" rel="noopener noreferrer">Raising cybersecurity awareness among children and parents using gamification</a>, Rolf Erik Sesseng Aas and Sigurd Røstad Augdal, July 2022, Master Thesis.</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/miabjo_ntnu_no/EdcxqKg0uDNJurGhPh6FgOcBIGPY9qXQslopf1X9wHGxbQ?e=GQdxIh" target="_blank" rel="noopener noreferrer">Exploring the IOTA Tangle for Health Data Management </a>, Eivind Solberg Rydningen and Erika Åsberg, June 2022, Master Thesis.</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/miabjo_ntnu_no/Ec1aGdrC_EJIlzNzNP-hgUoB2V1_AH4vdzCpFk9Fyae7WQ?e=u9ZdAb" target="_blank" rel="noopener noreferrer">Addressing Gender Inequality in Software Development Processes</a>, Sunniva Block and Hanne Olssen, June 2022, Master Thesis.</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/miabjo_ntnu_no/EQq6SY9FYxtLtPpHEQRC4hkBAse8CH69b-9OUqSjfcEsVQ?e=CEAJ7o" target="_blank" rel="noopener noreferrer">Toward Gender Diversity in Tech Entrepreneurship: Empirical Study on Female Entrepreneurs and a Software Design Solution Proposal</a>, Alis Wiken Wilson, June 2022, Master Thesis.</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/miabjo_ntnu_no/ETtE0hDNrChOkfpKNHSD3scBM6bAc5yqWS7kCraq446j1A?e=gfBvyN" target="_blank" rel="noopener noreferrer">Fighting child marriage in developing countries: design and development of a mobile application</a>, Ilaria Crivellari, June 2022, Master Thesis.</li>
    </ul>
  </>
)

export const Autumn21 = (
  <>
    <Subheading>Autumn Projects 2021</Subheading>
    <ul>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/marhh_ntnu_no/EX_c4_z_E2VEjKe-oN-8v-QB5rWS_E5qdtCV1vhHKanSOw?e=o56E79" target="_blank" rel="noopener noreferrer">Health Data Management & the IOTA Tangle: A Systematic Mapping Study</a>, Eivind Solberg Rydningen and Erika Åsberg, December 2021, IT3915 - Master in Informatics, Preparatory Project</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/marhh_ntnu_no/EceWCU6oDOBDnalcVVqeGVAB1ntY2aSQpx23rrlB7LMdzg?e=ydbXgN" target="_blank" rel="noopener noreferrer">Gender Equality in Tech Entrepreneurship: A Systematic Mapping Study</a>, Alis Wiken Wilson, December 2021, IT3915 - Master in Informatics, Preparatory Project</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/marhh_ntnu_no/ESu__4Zdn79OtFFKPXRfRYMBy0JuOHtSanWp-nzqbhLpjw?e=UtNHBc" target="_blank" rel="noopener noreferrer">Gender Equality in Information Technology Processes: A systematic Mapping Study</a>, Sunniva Block, December 2021, TDT4501 - Computer Science, Specialization Project</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/marhh_ntnu_no/ESkvGrDjhwBCstgotsK8vPMBZuLp9tfEDHjh-MGNAtdp3A?e=okbdxy" target="_blank" rel="noopener noreferrer">Detecting Gender Stereotypes using Digital StoryTelling</a>, Rahimu Kasibante, December 2021, TDT4501 - Computer Science, Specialization Project</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/marhh_ntnu_no/EU02OhpH9QBOrgmMFXUSbe0BOA6F0kT63tBlZhRQ5HQx2w?e=K7praV" target="_blank" rel="noopener noreferrer">Designing Software To Raise Cyber Security Awareness In Developing Countries: A Systematic Literature Review</a>, Giske Naper Freberg, December 2021, TDT4501 Computer Science, Specialization Project</li>
      <li><a href="https://studntnu-my.sharepoint.com/:b:/g/personal/marhh_ntnu_no/ESlfMgI0aOtKsrUvtsjoV7wBBONhveynM4aODMFJOij-xQ?e=z5MARB" target="_blank" rel="noopener noreferrer">Development of cybersecurity awareness solutions for children, using gamification: A Systematic Literature Review</a>, Rolf Erik Sesseng Aas and Sigurd Røstad Augdal, December 2021, TDT4501 - Computer Science, Specialization Project</li>
    </ul>
  </>
)

export const Spring21 = (
  <>
    <Subheading>Master Projects 2021</Subheading>
    <ul>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2020/12/prosjektoppgave_melzer_roarsen.pdf" target="_blank" rel="noopener noreferrer">Towards Suitable Free-to-Play Games for Children </a>, Anna Kristine Roarsen and Andreas Kristiansen Melzer, June 2021, Master thesis.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2022/01/marte___master_thesis.pdf" target="_blank" rel="noopener noreferrer">Creativity in Software for Children</a>,  Marte Hoff Hagen, June 2021, IT3906 - Informatics Postgraduate Thesis: Interaction Design, Game and Learning Technology.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2022/01/masteroppgave___betina-1.pdf" target="_blank" rel="noopener noreferrer">Software for children with ADHD</a>, Betina Pedersen Bye, August 2021, Master Thesis.</li>
    </ul>
  </>
)

export const Autumn20 = (
  <>
    <Subheading>Autumn Projects 2020</Subheading>
    <ul>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2020/12/prosjektoppgave_melzer_roarsen.pdf" target="_blank" rel="noopener noreferrer">Freemium Games For Children: A Systematic Literature Review</a>, Anna Kristine Roarsen and Andreas Kristiansen Melzer, December 2020, TDT4501 - Computer Science, Specialization Project</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2020/12/betina.pdf" target="_blank" rel="noopener noreferrer">Software for ADHD: A Systematic Literature Review</a>, Betina Pedersen Bye, December 2020, TDT4501 - Computer Science, Specialization Project.</li>
    </ul>
  </>
)

export const Master19 = (
  <>
    <Subheading>Master theses 2019</Subheading>
    <ul>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/08/engebak-masters-thesis.pdf" target="_blank" rel="noopener noreferrer">A digital game using collaborative storytelling to help children practice empathy</a>, Ina-Marie Hansen Engebak, June 2019, Master’s Thesis.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/07/masters_thesis__farzana_quayyum.pdf" target="_blank" rel="noopener noreferrer">The Role of Big Data in Addressing Societal Challenges</a>, Farzana Quayyum, June 2019, Master’s Thesis.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/07/template_for_a_masters_or_doctoral_thesis-1.pdf" target="_blank" rel="noopener noreferrer"> Scalability of Blockchain-based Supply Chain Management Systems </a>, Eirik H. Lund, June 2019, Master’s Thesis.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/06/ida.pdf" target="_blank" rel="noopener noreferrer">Creating an App Motivating People with Intellectual Disabilities to Do Physical Activity</a>, Ida Wold, June 2019, Master’s Thesis.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/06/ingrid-jens.pdf" target="_blank" rel="noopener noreferrer"> Designing game-inspired mobile applications to promote physical activity for individuals with intellectual disabilities</a>, Ingrid Evensen and Jens Brandsgård Omfjord, June 2019, Master’s Thesis.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/06/jostein.pdf" target="_blank" rel="noopener noreferrer"> Designing Mobile Software to Prevent Child Marriage</a>, Jostein Breivik, June 2019, Master’s Thesis</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/06/kshitiz_adhikari.pdf" target="_blank" rel="noopener noreferrer"> Exploring girls’ perception on Computing careers via educational quiz-application</a>, Kshitiz Adhikari, June 2019, Master’s Thesis.</li>
    </ul>
  </>
)

export const Autumn18 = (
  <>
    <Subheading>Autumn Projects 2018</Subheading>
    <ul>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/01/eirik-final.pdf" target="_blank" rel="noopener noreferrer">On The Relation Between Blockchain Technology and Sustainability: A Mapping Study</a>, Eirik Harald Lund, December 2018, TDT4501 - Computer Science, Specialization Project.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/01/farzana.pdf" target="_blank" rel="noopener noreferrer">The role of big data and analytics in addressing societal challenges: A systematic mapping review of the literature</a>, Farzana Quayyum, December 2018, TDT4501 - Computer Science, Specialization Project.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/01/ida.pdf" target="_blank" rel="noopener noreferrer">Exergame or Game-Inspired Application Motivating to Outdoor Physical Activity for Adolescents and Young Adults with Intellectual Disabilities </a> Ida Wold, December 2018, TDT4501 - Computer Science, Specialization Project.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/02/project-report-final.pdf" target="_blank" rel="noopener noreferrer">A collaborative digital game using storytelling to foster empathy in children </a> Ina-Marie Hansen Engebak, December 2018, TDT4501 - Computer Science, Specialization Project.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/01/jostein.pdf" target="_blank" rel="noopener noreferrer">Designing Mobile Software to Prevent Child Marriage</a>, Jostein Breivik, December 2018, TDT4501 - Computer Science, Specialization Project.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2019/01/kshitiz_adhikari.pdf" target="_blank" rel="noopener noreferrer">Serious Games and Preteen girls’ perception of Computer Science careers: A Systematic Review. </a>Kshitiz Adhikari, December 2018, TDT4501 - Computer Science, Specialization Project.</li>
    </ul>
  </>
)

export const Master18 = (
  <>
    <Subheading>Master Projects 2018</Subheading>
    <ul>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2018/09/masteroppgave.pdf" target="_blank" rel="noopener noreferrer">Creating an Effective Exergame with Lasting Entertainment Value</a>, Kristoffer Hagen and Stian Weie, June 2018, Master’s Thesis (Datateknologi 30 ECTS).</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2018/09/jie-masteroppgave.pdf" target="_blank" rel="noopener noreferrer">GQCCM: An evaluation strategy for collaborative social innovation platforms</a> Jie Li, June 2018, Master’s Thesis (Informatikk 60 ECTS).</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2018/09/masteroppgave-berg-birkeland.pdf" target="_blank" rel="noopener noreferrer">Exploring Empirical Engineering Approaches in Startup Companies: The Trilateral Hardware Startup Model. </a>Vebjørn Berg and Jørgen Birkeland, June 2018, Master’s Thesis (Datateknologi 30 ECTS).</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2018/09/sindre-masteroppgave.pdf" target="_blank" rel="noopener noreferrer">Tappetina's Empathy</a>, Sindre Berntsen Skaraas, June 2018, Master’s Thesis (Informatikk 60 ECTS).</li>
    </ul>
  </>
)

export const Autumn17 = (
  <>
    <Subheading>Autumn Projects 2017</Subheading>
    <ul>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2018/09/vebjorn-jorgen-project_thesis_autumn_2017.pdf" target="_blank" rel="noopener noreferrer">Software Startup Engineering: A Systematic Mapping Study</a>, Vebjørn Berg and Jørgen Birkeland, December 2017, TDT4501 - Computer Science, Specialization Project.</li>
      <li><a href="https://letiziajaccherihp.files.wordpress.com/2018/09/designing-an-effective-exergame-with-lasting-entertainment-value-002.pdf" target="_blank" rel="noopener noreferrer">Designing an effective exergame with lasting entertainment value (002)</a>, Kristoffer Hagen and Stian Weie, December 2017, TDT4501 - Computer Science, Specialization Project.</li>
    </ul>
  </>
)

import React from 'react';
import { Row, CustomWidthWrapper, Wrapper } from '../../components/globals';
import { TopicWrapper, Topic } from './elements';
import phd      from './static/phd.svg';
import master   from './static/master.svg';
import teaching from './static/teaching.svg';
import team     from './static/team.svg';
import projects from './static/projects.svg';
import videos   from './static/videos.svg';

// Main funtion
export default function TopicSelection() {
  return (    
    <Wrapper>
      <Row>
        <CustomWidthWrapper MaxWidth='700px'>
          <TopicWrapper>
            <Topic href='/phd' back='orange'><img src={phd} alt="phd"/> <p>PhD and Postdoctoral Students</p></Topic>
            <Topic href='/master' back='#1f97d4'><img src={master} alt="master"/> <p>Master Students</p></Topic>
            <Topic href='/teaching' back='#4ca146'><img src={teaching} alt="teaching"/> <p>Teaching</p></Topic>
            <Topic href='/team' back='#e01483'><img src={team} alt="team"/> <p>Team</p></Topic>
            <Topic href='/projects' back='#13496b'><img src={projects} alt="projects"/> <p>Projects</p></Topic>
            <Topic href='/videos' back='#fbc412'><img src={videos} alt="videos"/> <p>Videos</p></Topic>
          </TopicWrapper>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
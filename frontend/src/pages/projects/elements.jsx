import styled from "styled-components";

// Table for active projects
export const ActiveProjectsTable = styled.table`
  border: 0;
  width: 100%;
  border-collapse: collapse;
  text-align: left;
  margin: 0 0 1rem 0;
  overflow-x: auto;
  & tr {
    border-top: 1px grey solid;
    border-bottom: 1px grey solid;
  }
`

// Projects link
export const ProjectsLink = styled.a`
  display: inline-block;
  cursor: pointer;
  color: inherit;
  text-decoration: none;
  border-radius: 4px;
  text-align: center;
  margin: 1rem;
  transition: 0.2s;
  & h4 {
    margin: 10px 0;
    color: white;
    background-color: #000000d4;
    padding: 8px 10px;
  }
  & img {
    border-radius: 0;
    object-fit: cover;
    width: 280px;
    height: 200px;
  }
  &:hover {
    transform: scale(1.05);
  }
`